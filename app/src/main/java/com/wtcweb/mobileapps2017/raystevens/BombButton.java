package com.wtcweb.mobileapps2017.raystevens;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wtcweb.mobileapps2017.R;

import java.lang.reflect.Array;

public class BombButton extends AppCompatActivity {
    int x = 0;
    Button b;
    String arr[] = new String[3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arr[0]="SAVE ";
        arr[1]="EDIT";
        arr[2]="DELETE";
        setContentView(R.layout.activity_bomb_button);
        b = (Button)findViewById(R.id.button);
    }
    public void buttonPressed(View view){
        //b.setText("SAVE");
        x++; if(x > 2) x=0;
        b.setText(arr[x]);
    }
}
